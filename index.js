const express = require('express')
const app = express()
const mongoose = require('mongoose')
const keys = require('./config/keys')

mongoose.connect(keys.mongoURI, { useNewUrlParser: true, useUnifiedTopology: true }, (req, res) => {
  console.log("Mongo DB connects succesfully")
})

app.get('/', (req, res) => {
  res.send("Server Is running")
})


const PORT = process.env.PORT || 5000
app.listen(PORT, (req, res) => {
  console.log(`Server in running on port ${PORT}`)
})

